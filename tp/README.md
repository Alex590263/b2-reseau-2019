# TPs

* [TP1 : Remise dans le bain, clients et routeurs Linux, métrologie](./1)
* [TP2 : ARP, Switching (VLAN, STP, LACP)](./2)
* [TP3 : Router-on-a-stick](./3)
* [TP4 : TP buffet à volonté](./4)

## Général

* le but est de vous faire pratiquer. N'hésitez pas à dériver sur des choses qui vous intéressent un peu plus, en restant sur le thème du TP. Si c'est ennuyant, rendez le truc intéressant pour vous
* faites vos recherches en anglais, mettez votre OS en anglais (au moins vos VMs)
* n'hésitez pas à me MP Discord ou IRL en cas de besoin
* préférez les copier/coller aux screens
* n'utilisez PAS de connexion console à vos machines (mais plutôt SSH par exemple)
* côté GNU/Linux, on utilise que CentOS8 en cours
  * si vous utilisez autre chose, faut me le justifier

**Allez à votre rythme, prenez le temps de comprendre.**  
**Posez des questions.**  
**Prenez des notes au fur et à mesure.**  
**Lisez les parties en entier avant de commencer à travailler dessus.**  
**Smile** 🌞

## Rendu de TP

* format **Markdown uniquement**
* ***via* un dépôt `git`** (plateforme de votre choix, Github, Gitlab, autres)
* soyez complets et clairs
  * souvent un copier/coller de la ligne de commande et une petite ligne d'explication suffisent
* sachez aussi être concis
  * ça m'intéresse pas forcément d'avoir TOUTES les étapes de vos manipulations sauf quand explicitement demandé
* **les éléments précédés d'un 🌞 doivent absolument figurer dans le rendu**. Si c'est dans une bullet-list, tous les éléments doivent être rendus. Dans l'exemple qui suit, détail 1 et détail 2 doivent aussi figurer sur le rendu :
```
* 🌞 Objectif global
  * détail 1
  * détail 2
```
* **les éléments précédés d'un 🐙 sont des objectifs bonus**. A rendre si t'es cho.

* **les TPs sont à rendre *via* MP sur discord avec la chaîne `NOM,URL`**
  * c'est le nom pas le prénom
  * l'`URL` doit pointer vers votre rendu directement, ou il doit être simple à trouver (une page avec tous les rendus et des liens vers les rendus c'est OK du moment que c'est clair)
  * si la chaîne `NOM,URL` est mal formatée je prends pas, vous êtes grands now
  * un rendu par groupe avec un fichier `members` (sans extension de préférence) pour chaque TP
    * le fichier contient un NOM par ligne (et c'est tout)

## Côté technique

* toutes vos machines doivent avoir un nom de domaine défini
* dès que possible, utiliser une connexion à distance (comme SSH) pour se connecter aux machines
* vous **DEVEZ** fournir un plan d'adressage sous la forme de votre choix dès qu'il y a plusieurs machines
