# TP4 : Buffet à volonté

# Prérequis

Ca va dépendre des sujets. Mais globalement :
* [GNS3 fonctionnel](../../memo/setup_gns3.md)
* iOS Cisco routeur
  * c'est avec un [c3745](https://drive.google.com/drive/folders/1hnOwFTcEYeznsBwjFCzKbDripnCLOJSQ) qu'on a eu le moins de pb cette année
  * ou un [c7200](https://drive.google.com/drive/folders/17F80VfaHEMKWLHRc9aHn60AmjizXeb-R) si votre PC tient la route
* [IOU L2 Cisco](https://www.gns3.com/marketplace/appliance/iou-l2)
  * si juste l'import de cette appliance ne marche pas, vous pouvez directement download le `.bin` [sur le même drive que l'an dernier](https://drive.google.com/drive/folders/1LBIlztgGVAk4XsAeovKb1JHBrRg8QPSz).
* une machine CentOS7 prête à être clonée
  * pour tous les services qui tournent sous GNU/Linux

# Déroulement

Y'a un **sujet global** que tout le monde doit faire : monter un réseau avec *router-on-a-stick* comme au [TP 3](../3/).  
**On se servira de cette infra comme base pour monter le reste.**

Pour la suite, plusieurs sujets au choix. Vu qu'on a que deux jours, on va essayer de rendre le truc un peu cool, en vous laissant libres du sujet sur lequel vous bossez. 

Le but étant de vous faire découvrir de nouvelles choses, n'hésitez pas à au moins lire tous les sujets. 

**Tous les sujets sont à votre portée.**

J'apporterai des cours complémentaires si nécessaire sur les notions abordées dans les sujet au choix.

**L'idée :**
* Day 1 : fin de session, tout le monde a *au moins* l'infra du sujet global
* Day 2 : tout le monde bosse sur le sujet de son choix

# Rendu

Pour le rendu, le plus complet/économe en temps, je vous conseille : 
* vous faites `show run` sur vos équipements réseau et vous me copier-collez la conf dans des documents séparés 
  * si vous êtes des gens biens, vous enlevez la conf par défaut et vous me laissez que ce que vous avez ajouté
* dans le `README.md` de rendu, vous me mettez des liens vers ces fichiers
* vous me faites deux trois pings de démo/vérification (depuis des clients : récupération d'IP en DHCP si vous l'avez monté, ping Internet, ping dans le LAN)

# Index 

<!-- vim-markdown-toc GitLab -->

* [Sujet global : refresh](#sujet-global-refresh)
    * [La topo](#la-topo)
        * [Schéma GNS](#schéma-gns)
        * [Tableau des réseaux](#tableau-des-réseaux)
        * [Tableau d'adressage](#tableau-dadressage)
        * [Checklist](#checklist)
        * [Hints](#hints)
            * [Conf DHCP](#conf-dhcp)
            * [Conf DNS](#conf-dns)
            * [Conf serveur web](#conf-serveur-web)
        * [Solver](#solver)
* [Sujet au choix](#sujet-au-choix)
    * [1. Métrologie Réseau : SNMP, monitoring, gestion de logs](#1-métrologie-réseau-snmp-monitoring-gestion-de-logs)
    * [2. Sécurité attack & defense : spoofing, VLAN attack](#2-sécurité-attack-defense-spoofing-vlan-attack)
        * [A. Offensive](#a-offensive)
            * [ARP Spoofing](#arp-spoofing)
            * [DHCP Spoofing](#dhcp-spoofing)
            * [DNS Spoofing](#dns-spoofing)
            * [Messing up with VLANs](#messing-up-with-vlans)
        * [B. Defensive](#b-defensive)
            * [ARP Spoofing](#arp-spoofing-1)
            * [DHCP Spoofing](#dhcp-spoofing-1)
            * [Messing up with VLANs](#messing-up-with-vlans-1)
    * [3. Archi réseau : déploiement automatisé, git, sauvegarde de conf](#3-archi-réseau-déploiement-automatisé-git-sauvegarde-de-conf)
    * [4. DNS is levelling up](#4-dns-is-levelling-up)
    * [5. Anonymat en ligne](#5-anonymat-en-ligne)
    * [Your choice](#your-choice)
* [D'autres idées ?](#dautres-idées-)
    * [Sécurisation d'un accès front/distant](#sécurisation-dun-accès-frontdistant)
    * [Réalisation d'une compote maison](#réalisation-dune-compote-maison)
    * [Where's my FOSS ? :'(](#wheres-my-foss-)

<!-- vim-markdown-toc -->

# Sujet global : refresh

**Petit rafraîchissement de mémoire**, qui est je pense, nécessaire. Nan ? On va monter une tite infra avec un *router-on-a-stick*.

Le but : monter une tite infra avec des fonctionnalités élementaires d'architecture réseau :
* *VLAN* : isolation réseau L2
* *DNS* : résolution de noms locale
* *DHCP* : attribution d'IP locales (et autres informations liées au LAN)
* *NAT* : routage entre domaine privé et public

## La topo

### Schéma GNS

<div align="center"><img src="./pic/topo.png" /></div>

### Tableau des réseaux

| Name     | Address        | VLAN |
|----------|----------------|------|
| `admins` | `10.5.10.0/24` | 10   |
| `guests` | `10.5.20.0/24` | 20   |
| `infra`  | `10.5.30.0/24` | 30   |

### Tableau d'adressage

| Machine  | `admins`      | `guests`      | `infra`       |
|----------|---------------|---------------|---------------|
| `r1`     | `10.5.10.254` | `10.5.20.254` | `10.5.30.254` |
| `admin1` | `10.5.10.11`  | x             | x             |
| `admin2` | `10.5.10.12`  | x             | x             |
| `admin3` | `10.5.10.13`  | x             | x             |
| `guest1` | x             | `10.5.20.11`  | x             |
| `guest2` | x             | `10.5.20.12`  | x             |
| `guest3` | x             | `10.5.20.13`  | x             |
| `dhcp`   | x             | `10.5.20.253` | x             |
| `dns`    | x             | x             | `10.5.30.11`  |
| `web`    | x             | x             | `10.5.30.12`  |


### Checklist

> 📚 Le [mémo Cisco](../../memo/cisco_memo.md) est toujours dispo.

**1.** setup la topo dans GNS  
**2.** mettre en place les VLANs  
**3.** définir les IPs statiques des admins, guests et routeurs  
**4.** (optionnel-mais-conseillé) mettre en place le serveur DHCP  
**5.** (optionnel-mais-conseillé) mettre en place le serveur DNS  
**6.** (optionnel) mettre en place le serveur Web  

N'oubliez pas de tout vérifier :
* les clients du réseau se joignent
* les clients du réseau joignent le WAN
* optionnels :
  * le serveur Web est joignable
  * il est possible de requêter le serveur DNS en local
  * les clients peuvent récupérer *via* le DHCP
    * une IP
    * l'adresse du serveur DNS
    * l'adresse de la passerelle

### Hints

#### Conf DHCP

* serveur CentOS7
* installer le paquet `dhcp`
* configurer `/etc/dhcp/dhcpd.conf`
  * [exemple de conf](./dhcp/dhcpd.conf)
* démarrer le service `dhcpd`

Tester avec un client la récupération d'un IP et l'addresse de la passerelle, (et adresse de serveur DNS).

#### Conf DNS

* installer les paquets `bind` (serveur DNS) et `bind-utils` (outils DNS)
* configurer le serveur DNS en suivant les exemples de configuration
  * [`/etc/named.conf`](./dns/etc/named.conf)
    * fichier de configuration de bind
  * [`/var/named/tp4.b2.db`](./dns/var/named/tp4.b2.db)
    * fichier de zone
    * détermine quels noms de domaine seront traduisibles en IP
  * [`/var/named/20.5.10.db`](./dns/var/named/20.5.10.db)
    * fichier de zone inverse
    * détermine quelles IPs pouront être traduites en noms de domaine
  * [exemples de conf](./dns)
* il faudra [ouvrir certain(s) port(s) dans le firewall](../../memo/linux_network_memo.md#interagir-avec-le-firewall)
  * la commande [`ss`](../../memo/linux_network_memo.md#netstat-ou-ss) permet de lister les ports en écoute
* démarrer le service

Testez la résolution de noms :
* avec [dig](#nslookup-ou-dig)
* ou de simples `ping <NAME>` depuis les VPCS de GNS

#### Conf serveur web

* installer le paquet `epel-release` puis `nginx`
* démarrer le service `nginx`
* [ouvrir le port](../../memo/linux_network_memo.md#interagir-avec-le-firewall) `80/tcp`

Tester que les clients accèdent correctement au site Web.
* vous pouvez utiliser un client CentOS7

### Solver

<div align="center"><img src="./pic/topo_with_nic_names.png" /></div>

Pour les équipements Cisco, vous trouverez les confs dans les fichiers texte associés à chaque équipement : 
* [R1](./solver/conf_cisco/r1.txt)
* [client-sw1](./solver/conf_cisco/client-sw1.txt)
* [client-sw2](./solver/conf_cisco/client-sw2.txt)
* [client-sw3](./solver/conf_cisco/client-sw3.txt)
* [infra-sw1](./solver/conf_cisco/infra-sw1.txt)

# Sujet au choix

## 1. Métrologie Réseau : SNMP, monitoring, gestion de logs

**But : on veut garder un oeil sur le réseau**

C'est à dire...
* connaître l'état des équipement réseaux
* avoir une vue sur les flux réseau
* éditer une carte en temps réel du réseau

Pour ça, on va setup [Observium](https://www.observium.org/), qui repose en grande partie sur le protocole SNMP :
* FOSS
* permet de gérer la communication SNMP avec les équipements réseau
  * gère le monitoring SNMP
  * dessine des cartes du réseau 
* utilise des protocoles standards

La doc d'Observium est plutôt cool, je vais pas faire une redite ici.  
Il va vous falloir un serveur web et une base de données.

## 2. Sécurité attack & defense : spoofing, VLAN attack

**But : hackers gonna hack (or not ?)**
* mettre en place des attaques réseau
* mettre en place des contremesures

### A. Offensive 

#### ARP Spoofing

Usurpation d'identité L2/L3 : on usurpe l'adresse IP d'une machine du réseau en empoisonnant la table ARP de la victime.

Je recommande :
* [la commande `arping`](https://sandilands.info/sgordon/arp-spoofing-on-wired-lan)
* la librairie Python `scapy`
  * vous trouverez BEAUCOUP d'exemples sur internet

Le concept :
* l'attaquant envoie un ARP request (en broadcast donc)
  * il demande l'IP de la victime et précise
    * sa vraie MAC en source
    * une fausse IP en source
* la victime reçoit le message
  * la victime répond à l'attaquant avec un ARP reply (l'attaquant obtient l'adresse MAC de la victime)
  * **la victime inscrit dans sa table ARP les infos de la trame reçue** :
    * désormais la victime pense que l'IP envoyée (fausse) correspond à la MAC envoyée (celle de l'attaquant)
* répéter l'échange pour maintenir l'attaque en place
  * les entrées dans les tables ARP sont temporaires (de 60 à 120 secondes en général)

Pour mettre en place un man-in-the middle :
* l'attaquant usurpe l'IP de la victime auprès de la passerelle du réseau
* l'attaquant usurpe l'IP de la passerelle auprès de la victime
* tous les messages que la victime envoie vers d'autres reéseaux (internet par exemple) passent par l'attaquant

#### DHCP Spoofing

Mettre en place un rogue DHCP, aussi appelé "DHCP server of the doom" *(padutou)*.  
On met en place un serveur DHCP, et on file des IPs aux clients. On tire alors profit des *options DHCP* afin de fournir d'autres fausses infos aux clients. Principalement : adresse du serveur DNS du réseau, adresse de la passerelle.

On peut donc définir arbitrairement l'adresse de passerelle et celle du DNS pour les clients. Cela mène à des attaques Man-in-the-middle et DNS spoofing.

#### DNS Spoofing

Il est nécessaire de faire l'ARP spoofing ou le DHCP spoofing pour ça.

Effectuer des réponses DNS fallacieuses. Une fois l'identité du serveur DNS ou de la passerelle d'un réseau usurpée, on peut répondre aux requêtes DNS des clients. 

#### Messing up with VLANs

Forcer un switch à négocier un trunk pour récupérer tout le trafic et envoyer des messages à tout le réseau.

Une fois le trunk négocié, il est possible de :
* voir toutes les trames broadcast envoyées (entre autres)
  * donc on récupère des MAC, des IPs, et le VLAN associé
* envoyer des trames dans n'importe quel VLAN
  * enfin, n'importe quel VLAN auquel le switch avec qui on a négocié a accès

### B. Defensive

#### ARP Spoofing

Mise en place d'un *IP source guard*.

#### DHCP Spoofing

Mise en place de *DHCP snooping* qui permet d'interdire des trames DHCP sur les ports non-autorisés.

#### Messing up with VLANs

Mise en place de contre-mesures contre les attaques liées aux VLANs :
* désactiver explicitement les ports non utilisés
  * *a minima* désactiver la négociation
* le mécanisme *IP source guard* peut aussi aider

## 3. Archi réseau : déploiement automatisé, git, sauvegarde de conf

**But : gérer la configuration réseau de façon automatisée avec des outils modernes.**

> Il paraît que vous avez aimé Ansible ? Vous en voulez encore ? "Ouais ouais ouais !!!!". Alors en voilà un peu plus.

Ici on va mettre en place :
* accès distant SSH aux équipements
* déploiement automatisé de conf Cisco avec Ansible
* backup de configuration dans `git`

**[ 📚 Voir le document associé](./cisco_ansible.md).**

## 4. DNS is levelling up

**But : mettre en place une tite archi DNS**

Ce dont on parle ici c'est...
* serveurs DNS redondés
* capables de résoudre
  * des noms internes
  * des noms externes
* utilisation d'un IPAM
  * un DNS c'est bien
  * avec un IPAM c'est mieux
  * un IPAM *(IP address management)* permet de gérer plus simplement des enregistrement DNS
  * parce qu'écrire des fichiers de zone c'est chiant
  * [Netbox](https://netbox.readthedocs.io/en/stable/) est vraiment très puissant (et vraiment sexy aussi)
* fonctionnalités de sécu avancées
  * DNSSEC
  * DoH/DoT
    * respectivement *DNS over HTTPS* et *DNS over TLS*

Quelques serveurs DNS de référence :
* [PowerDNS](https://www.powerdns.com/)
* [bind](https://www.isc.org/bind/) (oui oui le site c'est bien l'ISC)
* le couple [Unbound](https://nlnetlabs.nl/projects/unbound/about/) + [NSD](https://www.nlnetlabs.nl/projects/nsd/about/)

## 5. Anonymat en ligne

**But : comment garantir ou renforcer son anonymat en ligne**

L'idée est de passer en revue et comparer différents outils qu'on peut mettre en place pour renforcer son anonymat en ligne.

Au menu :
* proxy
* VPN
* client Tor
* hidden service Tor
* DoH/DoT (DNS over HTTPS, DNS over TLS)

Voir [le document dédié](./anon.md).

## Your choice

**Vous pouvez aussi me proposer un sujet de votre choix.** 

# D'autres idées ?

J'ai aussi d'autres sujets qui poussent, mais ils sont encore jeunes.

> Qu'est-ce que j'entends par "jeune" ? Rien, cette analogie n'a pas de sens. C'est même pas une analogie en fait. Ce sont des sujets soit un peu troll, soit des sujets que je considère moins intéressants pour vous, soit que c'est une bonne idée mais c'est trop court un ou deux jours (ou alors j'ai pas assez de skillz :'( ).

## Sécurisation d'un accès front/distant

* mise en place d'un firewall (ou cluster de firewalls) en frontal de l'infra : pfSense
* sécurisation des accès SSH version hardcore
* mise en place d'un accès VPN (Wireguard, ô beau Wireguard)
* gestion fine
  * des accès utilisateurs
  * des zones réseau accédées
* gestion des logs d'accès

## Réalisation d'une compote maison

* pas trop de sucre, c'est pas bon
* il vous faut **des bonnes pommes**
* vous pouvez ajouter d'autres fruits, mais une base pomme c'est toujours bien
* ma préférence perso : pomme, poire, banane, vanille, les vrais savent
  * proportions : 
    * 4 pommes
    * 1 poire
    * 1 banane
    * 1/2 gousse de vanille (ou 1 full gousse si vous êtes des fus)
    * 2 cuillères à soupe rase de sucre (max)

> Astuce de mamie : utiliser une casserole en fonte.

* recette
  * mettre tout ça dans une casserole, feu doux, avec un fond d'eau
  * couvrir et laisser vingt bonnes minutes (vérifier au fur et à mesure)
  * quand c'est tout fondu et que ça sent tout bon
    * écraser au fouet/écrase-patates (ou fourchette si vous êtes pauvres)
    * ou mixer avec un robot ou mixeur plongeant
  * stocker dans un récipient en verre 
    * bien lavé 
    * voire stérilisé si vous êtes des gens biens
    * **mais en vrai balec, c'est tellement bon, en deux jours y'en a pu t'façon**
  * laisser refroidir à l'air
  * stocker au frigo
  * **tout manger**

> Quand ce sera la saison on y ajoutera des fruits rouges, et ce sera carton total.

## Where's my FOSS ? :'(

* monter la même infra mais exclusivement avec de l'OpenSource
* technos
  * [OpenVSWitch](https://www.openvswitch.org/) pour le switching
  * [vyOS](https://www.vyos.io/) pour le routing
* why ? 
  * c'est de la base GNU/Linux donc on peut utiliser tous les outils d'admin standards
    * monitoring, backup, déploiement, maîtrise, etc.
  * éviter les boîtes noires et avoir une maîtrise de son infra
  * ne pas s'enfermer technologiquement
    * ni en terme de hardware
    * ni en terme de software
  * utilisation d'outils et protocoles standards
