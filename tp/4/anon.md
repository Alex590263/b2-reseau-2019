# Anonymat en ligne

<!-- vim-markdown-toc GitLab -->

* [Proxy HTTP](#proxy-http)
* [Tor](#tor)
    * [Connexion au web avec Tor](#connexion-au-web-avec-tor)
    * [Hidden service Tor](#hidden-service-tor)
        * [Création d'un Hidden Service](#création-dun-hidden-service)
* [DoH/DoT](#dohdot)
* [Bilan](#bilan)

<!-- vim-markdown-toc -->

# Proxy HTTP

Faites chauffer votre moteur de recherche préféré et cherchez un "free proxy https" pour tomber sur des sites [de ce genre](https://free-proxy-list.net/).

Configurez votre navigateur pour utiliser l'un de ces proxies HTTP et/ou HTTPS.

> Dans Firefox : Paramètres > Catégorie "Network Settings" > Bouton "Settings" > Manual configuration.

Une fois le proxy en place, essayez d'effectuer des requêtes. Proxy gratuit donc pas toujours OK, testez en un autre si ça passe pas.

🌞 Lancez Wireshark et observez le trafic émis lors d'un trafic utilisant un proxy HTTP, puis un proxy HTTPS.
* vous pouvez aussi tester en visitant des sites en HTTP ou HTTPS si nécessaire pour voir la différence.

# Tor

Tor est un outil qui a pour but de proposer un réseau web alternatif. Ce réseau web est très similaire au web classique, mais inclut, au coeur du protocole Tor (The Onion Router), des fonctionnalités natives afin d'augmenter le niveau de confidentialité des échanges.  
Cela permet notamment aux utilisateurs du réseau d'avoir une garantie d'un anonymat (beaucoup) plus fort qu'avec l'utilisation du web classique.

## Connexion au web avec Tor

Téléchargez le [Tor Browser](https://www.torproject.org/download/).

Une fois en possesion du Tor Browser, lancez-le et faites un peu de navigation.  
Globalement, c'est pareil, juste plus lent. Ha et ptet pas dans la même langue. 

En cliquant sur le cadenas vert, à gauche de la barre d'URL, vous pouvez voir le circuit utilisé pour effectuer la connexion. 

Le protocole Tor utilise un chiffrement fort et trois noeuds intermédiaires par lesquels transitent les requêtes avant d'être émises.  
Les trois noeuds choisis par le client Tor sont appelés un *circuit Tor*. Il est choisi aléatoirement et c'est l'utilisation de circuit qui garantit l'anonymat des clients. 

L'encart d'info du cadenas vert fait référence au terme *Guard Node*.  
Le *Guard node*, ou *guard relay*, *entry relay* est le premier des trois noeuds intermédiaires. C'est celui auquel le client (vous) effectue la connexion.

🌞 Lancez Wireshark et observez le trafic émis lors d'un trafic utilisant le Tor Browser, comparé à une connexion classique. 
* Testez avec un site en HTTP et HTTPS

## Hidden service Tor

Un *hidden service* c'est un service qui n'est pas disponible sur le web classique, mais uniquement en utilisant un navigateur compatible aec le protocole Tor.  
Ce dernier va nous permettre d'héberger des sites web classique au sein du réseau Tor, afin de renforcer grande l'anonymat du client comme du serveur.

Les échanges de clés sont nombreux lorsqu'un client se connecte à un hidden service, car le client crée toujours un circuit Tor, mais cette fois-ci, le serveur aussi.  
C'est donc un total de 6 noeuds intermédiaires. Plus exactement 7 noeuds, car le client et le serveur se rejoignent à un "point de rendez-vous" au sein du réseau Tor.

Il est très simple de créer un Hidden Service Tor. 

### Création d'un Hidden Service

Go sur une VM CentOS7.

```bash
# Mise à jour des dépôts
$ sudo yum install -y epel-release
$ sudo yum update -y 

# Installation d'un serveur web et de tor
$ sudo yum install -y nginx tor

# Modification de la page d'accueil NGINX pour l'identifier facilement
$ sudo vim /usr/share/nginx/html/index.html

# Démarrage du serveur NGINX
$ sudo systemctl enable --now nginx

# Modification de la configuration de tor
# Ajout d'un hidden service
$ sudo vim /etc/tor/torrc
# Dans /etc/tor/torrc, ajouter les lignes
HiddenServiceDir /var/lib/tor/hidden_service/
HiddenServicePort 80 127.0.0.1:80

# Création du répertoire de données pour tor
$ sudo mkdir /var/lib/tor/hidden_service/ -p
$ sudo chown -R toranon:toranon /var/lib/tor/hidden_service/
$ sudo chmod 700 -R /var/lib/tor/hidden_service/

# Démarrage et activation du hidden service
$ sudo systemctl restart tor
$ sudo systemctl enable tor

# Récupération de l'adresse du site
$ sudo cat /var/lib/tor/hidden_service/hostname
```

Une fois en possession de votre hostname en `.onion`, vous pouvez visiter votre hidden service avec le Tor Browser.

🌞 Lancez `tcpdump` sur la VM et récupérez la capture sur l'hôte afin de l'exploiter avec Wireshark :

```bash
$ sudo tcpdump -i <INTERFACE> -w <FILE>

# Par exemple
$ sudo tcpdump -i enp0s8 -w capture.pcap
```

> `.pcap` est l'extension standard pour les captures réalisées avec Wireshar ou `tcpdump`.

# DoH/DoT

> [Petit lien/article](https://hacks.mozilla.org/2018/05/a-cartoon-intro-to-dns-over-https/) vraiment top et abordable qui explique tout en détail.

Ces deux technologies permettent d'envoyer des requêtes DNS dans des tunnels chiffrés, afin de garantir, entre autre, la confidentialité des requêtes effectuées par les clients.

Configurez votre navigateur afin de mettre en place du DoH. 

🌞 Utilisez Wireshark pour déterminer la différence entre DNS classique et DoH.
* trouver l'adresse du serveur DNS utilisé

# Bilan

🌞 Faites un comparatif entre :
* proxy HTTP
* Tor
* Tor hidden servie
* DoH
* VPN (vu l'année passée)

Un tableau ferait bien le taf pour cette question.

Les critères importants sont :
* confidentialité de l'échange
* quel message 
  * un paquet IP ? 
    * IP source ? 
    * IP destination ?
    * si c'est en plusieurs étapes (VPN, Tor, ...), c'est pertinent d'analyser chaque étape si possible
  * quel protocole applicatif ?
    * TLS ? 
    * HTTPS ?
    * en tout cas, ce qu'on veut savoir : c'est chiffré ? Pas chiffré ? 
  * etc.
* anonymat du client
* anonymat du serveur
