# B2 - Réseau 2019

Ici seront hébergées l'ensemble des ressources liées au cours de Réseau 2ème année. 

Actuellement dispo : 
* [les TPs](/tp)
* [les mémos](/memo)
  * [Lexique](/memo/lexique.md)
  * [CLI Cisco](/memo/cisco_memo.md)
  * [Commandes réseau](/memo-commandes-linux.md) (Linux principalement, mais pas que)
  * [Setup GNS3](/memo/setup_gns3.md)
