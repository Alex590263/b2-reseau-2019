# Procédures

Vous trouverez ici quelques mini-procédures pour réaliser certaines opérations récurrentes. Ce sera évidemment principalement utilisé pour notre cours de réseau, mais peut-être serez-vous amenés à le réutiliser plus tard.

**Elles sont écrites pour un système Cisco**.

## Sommaire

<!-- vim-markdown-toc GitLab -->

* [Les modes du terminal](#les-modes-du-terminal)
* [Définir une IP statique](#définir-une-ip-statique)
* [Définir une IP dynamiquement](#définir-une-ip-dynamiquement)
* [Garder les changements après reboot](#garder-les-changements-après-reboot)
* [Ajouter une route statique](#ajouter-une-route-statique)
* [Changer son nom de domaine](#changer-son-nom-de-domaine)
* [Gérer sa table ARP](#gérer-sa-table-arp)
* [Configuration d'un NAT simple](#configuration-dun-nat-simple)
* [OSPF](#ospf)
* [VLAN](#vlan)
    * [Mode *access*](#mode-access)
    * [Mode trunk](#mode-trunk)
* [Sous-interface, routage inter-VLAN](#sous-interface-routage-inter-vlan)

<!-- vim-markdown-toc -->

---

### Les modes du terminal
Le terminal Cisco possède plusieurs modes

Mode | Commande | What ? | Why ?
--- | --- | --- | ---
`user EXEC` | X | C'est le mode par défaut : il permet essentiellement de visualiser des choses, mais peu d'actions à réaliser | Pour visualiser les routes ou les IPs par ex
`privileged EXEC` | enable | Mode privilégié : permet de réalisé des actions privilégiées sur la machine | Peu utilisé dans notre cours au début
`global conf` | conf t | Configuration de la machine | Permet de configurer les interface et le routage

L'idée globale c'est que pour **faire des choses** on passera en `global conf` pour **faire** des choses, et on restera en **user EXEC** pour **voir** des choses.

### Définir une IP statique
**1. Repérer le nom de l'interface dont on veut changer l'IP**
```
# show ip interface brief
OU
# show ip int br
```
**2. Passer en mode configuration d'interface**
```
# conf t
(config)# interface ethernet <NUMERO>
```
**3. Définir une IP**
```
(config-if)# ip address <IP> <MASK>
Exemple :
(config-if)# ip address 10.5.1.254 255.255.255.0
```
**4. Allumer l'interface**
```
(config-if)# no shut
```
**5. Vérifier l'IP**
```
(config-if)# exit
(config)# exit
# show ip int br
```
---


### Définir une IP dynamiquement

Une IP définie dynamiquement est une IP récupérée *via* DHCP.

**1. Repérer le nom de l'interface dont on veut changer l'IP**
```
# show ip interface brief
OU
# show ip int br
```
**2. Passer en mode configuration d'interface**
```
# conf t
(config)# interface ethernet <NUMERO>
```
**3. Définir une IP**
```
(config-if)# ip address dhcp
```
**4. Allumer l'interface**
```
(config-if)# no shut
```
**5. Vérifier l'IP**
```
(config-if)# exit
(config)# exit
# show ip int br
```

---

### Garder les changements après reboot
Les équipements Cisco possèdent deux configurations (d'une certain façon) :
* la `running-config`
  * c'est la conf actuelle
  * elle contient toutes vos modifications
  * `# show running-config` pour la voir
* la `startup-config`
  * c'est la conf qui est chargée au démarrage de la machine
  * elle ne contient aucune de vos modifications
  * `show startup-config`

Comment garder vos changements à travers les reboots ? Il faut copier la `running-config` sur la `startup-config` :
```
# copy running-config startup-config
```

---

### Ajouter une route statique

**1. Passer en mode configuration**
```
# conf t
```

**2.1. Ajouter une route vers un réseau**
```
(config)# ip route <REMOTE_NETWORK_ADDRESS> <MASK> <GATEWAY_IP>

Exemple : ajouter une route vers le réseau 10.1.0.0/24 en passant par la passerelle 10.2.0.254
(config)# ip route 10.1.0.0 255.255.255.0 10.2.0.254
```

**2.2. Ajouter la route par défaut**
```
(config)# ip route 0.0.0.0 0.0.0.0 10.2.0.254
```

**3. Vérifier la route**
```
(config)# exit
# show ip route
```

### Changer son nom de domaine
**1. Passer en mode configuration**
```
# conf t
```

**2. Changer le hostname**
```
(config)# hostname <HOSTNAME>
```

### Gérer sa table ARP

* voir sa table ARP
```
# show arp
```

### Configuration d'un NAT simple


Configuration des interfaces "externes" et "internes" :
* "externes" : les interfaces qui pointent vers le WAN
* "internes" : les interfaces qui pointent vers des LANs

**1. Repérage des interfaces "internes" et "externes"**
```
# show ip int br
```

**2. Passer en mode config**
```
# conf t
```

**3. Configurer les interfaces en "interne" ou "externe"**
```
Interfaces "externes" :
(config)# interface fastEthernet 0/0
(config-if)# ip nat outside
(config-if)# exit

Interfaces "internes" :
(config)# interface fastEthernet 1/0
(config-if)# ip nat inside
(config-if)# exit

(config)# interface fastEthernet 2/0
(config-if)# ip nat inside
(config-if)# exit
```

**4. Définir une liste où tout le trafic est autorisé**
```
(config)# access-list 1 permit any
```

**5. Configurer le NAT**
```
(config)# ip nat inside source list 1 interface <OUTSIDE_INTERFACE> overload
Par exemple :
(config)# ip nat inside source list 1 interface fastEthernet 0/0 overload
```

**6. UNIQUEMENT DANS LE CAS DE L'UTILISATION DE OSPF : partager la route par défaut dans OSPF**
```
(config)# router ospf 1
(config-router)# default-information originate
```

### OSPF

**1. Passer en mode configuration**
```
# conf t
```

**2. Activer OSPF**
```
(config)# router ospf 1
```
* le `1` correspond à l'ID de ce processus OSPF
* nous utiliserons toujours `1` pendant nos derniers cours

**3. Définir un `router-id`**
```
# dans nos TP, le router-id sera toujours le numéro du routeur répété 4 fois
# donc 1.1.1.1 pour router1
(config-router)# router-id 1.1.1.1
```

**4. Partager une ou plusieurs routes**
```
(config-router)# network 10.6.100.0 0.0.0.3 area 0
```
* cette commande partage le réseau `10.6.100.0/30` avec les voisins OSPF, et indique que ce réseau est dans l'`area 0` (l'aire "backbone")
* l'utilisation de cette commande est un peu particulière
* nous ne rentrerons pas dans les détails de fonctionnement (sauf si on a le temps) de OSPF
* **donc retenez simplement que pour le masque, vous devez écrire l'inverse de d'habitude**
* c'est à dire `0.0.0.3` au lieu de `255.255.255.252` par exemple

**Vérifier l'état d'OSPF** :
```
# show ip protocols
# show ip ospf interface
# show ip ospf neigh
# show ip ospf border-routers
```

### VLAN

#### Mode *access*

**Que sur des switches. N'utilisez pas le VLAN 1.**

Le mode *access* permet de définir un port d'un switch dans un VLAN donné. On dit du client qui est branché à ce port qu'il est "dans" ce VLAN.

**1. Passer en mode configuration**
```
# conf t
```

**2. Définir les VLANs à utiliser**
```
(config)# vlan 10
(config-vlan)# name admins
(config-vlan)# exit

(config)# vlan 20
(config-vlan)# name guests
(config-vlan)# exit

(config)# exit
# show vlan
```

**3. Attribuer à chaque interface du switch un VLAN**
```
# conf t
(config)# interface fastEthernet0/0
(config-if)# switchport mode access
(config-if)# switchport access vlan 10
(config-if)# exit
```

#### Mode trunk

Le mode *trunk* permet de définir quels VLANs seront autorisés à circuler entre deux équipements réseau, typiquement deux switches.

**1. Passer en mode configuration**
```
# conf t
```

**2. Définir les VLANs à utiliser**
```
(config)# vlan 10
(config-vlan)# name admins
(config-vlan)# exit

(config)# vlan 20
(config-vlan)# name guests
# show vlan
```

**3. Attribuer un trunk entre deux équipements**
```
# conf t
(config)# interface fastEthernet0/0
(config-if)# switchport trunk encapsulation dot1q
(config-if)# switchport mode trunk
(config-if)# switchport trunk allowed vlan add 10,20
(config-if)# exit
(config)# exit
# show interface trunk
```

### Sous-interface, routage inter-VLAN

La configuration de sous-interface permet :
* de mettre au courant le routeur qu'il doit utiliser des tags VLANs dans les trames qu'il route
* de faire du routage inter-VLAN

Pour chaque sous-interface, on définit :
* une IP
* le type d'encapsulation et l'ID de VLAN associé

**1. Passer en mode configuration**
```
# conf t
```

**2. Configurer deux sous-interfaces**
```
(config)# interface fastEthernet0/0.10
(config-subif)# encapsulation dot1Q 10
(config-subif)# ip address 10.10.10.254 255.255.255.0
(config-subif)# autre conf...
(config-subif)# exit

(config)# interface fastEthernet0/0.20
(config-subif)# encapsulation dot1Q 20
(config-subif)# ip address 10.10.20.254 255.255.255.0
(config-subif)# autre conf...
(config-subif)# exit
```

**3. Allumer l'interface-mère**
```
(config)# interface fastEthernet0/0
(config-if)# no shut
(config-if)# exit
```

**4. Vérification**
```
(config)# exit
# sh ip int br
```




