# Mémos

Vous trouverez ici les mémos utiles pour le cours de Réseau :
* [Lexique](/memo/lexique.md)
* [Mémo commandes](/memo/memo-commandes-linux.md) Linux principalement
* [Ligne de commande Cisco](/memo/cisco_memo.md)
* [Setup GNS3](/memo/setup_gns3.md)