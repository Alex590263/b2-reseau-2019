# Lexique

* [Protocoles](#protocoles)
  * [Couche 2](#couche-2-modèle-osi)
    * [Ethernet](#ethernet)
    * [LACP](#lacp-link-aggregation-control-protocol)
    * [STP](#stp-spanning-tree-protocol)
    * [VLAN](#vlan-virtual-local-area-network)
  * [Couche 2/3](#couche-23-du-modèle-osi)
    * [ARP](#arp-address-resolution-protocol)
      * [A. Concept](/memo/lexique.md#a-concept)
      * [B. Quand ARP est utilisé ?](/memo/lexique.md#b-quand-est-ce-que-arp-est-utilisé-)
      * [C. ARP request et ARP reply](/memo/lexique.md#c-arp-request-et-arp-reply)
      * [D. Adresse MAC de broadcast](/memo/lexique.md#d-adresse-mac-de-broadcast)
      * [E. Exemple](/memo/lexique.md#e-exemple-concret)
  * [Couche 3](#couche-3-du-modèle-osi)
    * [IP](#ip-internet-protocol-v4)
  * [Couche 4](#couche-4-du-modèle-osi)
    * [TCP](#tcp-transmission-control-protocol)
    * [UDP](#udp-user-datagram-protocol)
  * [Couches applicatives (5+)](#au-dessus-de-4-couches-applicatives-qui-transportent-de-la-donnée)
    * [DHCP](#dhcp-dynamic-host-configuration-protocol)
    * [DNS](#dns-domain-name-system)
    * [HTTP](#http-hypertext-transfer-protocol)
    * [SSH](#ssh-secure-shell)
* [Autres sigles et acronymes](#siglesacronymes)
  * [CIDR](#cidr-classless-inter-domain-routing)
  * [FQDN](#fqdn-fully-qualified-domain-name)
  * [LAN](#lan-local-area-network)
  * [MAC](#mac-media-access-control)
  * [RFC](#rfc-request-for-comments)
  * [WAN](#wan-wide-area-network) 
* [Notions](#notions)
  * [Adresse de réseau](#adresse-de-réseau)
  * [Adresse de diffusion](#adresse-de-diffusion-ou-broadcast-address)
  * [Binaire](#binaire)
  * [Carte Réseau](#carte-réseau-ou-interface-réseau)
  * [Client/Serveur](#clientserveur)
  * [Encapsulation](#encapsulation)
  * [IP privées/publiques](#ip-privéespubliques)
  * [Loopback](#loopback)
  * [Masque de sous-réseau](#masque-de-sous-réseau)
  * [Noms de domaine](#noms-de-domaine)
  * [Pare-feu/Firewall](#pare-feu-ou-firewall)
  * [Passerelle/Gateway](#passerelle-ou-gateway)
  * [Ports TCP/UDP](#ports)
  * [Routage](#routage-ou-routing)
  * [Routeur](#routeur)
  * ["Stack réseau" ou "Pile TCP/IP"](#stack-réseau-ou-stack-tcpip-ou-pile-réseau)
  * [Subnetting](#subnetting)
  * [Table de routage](#table-de-routage)

## Protocoles
### Couche 2 modèle OSI
#### *Ethernet*
* un *message Ethernet* est une **trame Ethernet**
* utilisé pour la *commutation de paquets*
* Ethernet définit le format des trames Ethernet qui circulent sur le réseau (entre autres)
  * [MAC](#mac-media-access-control) source d'un message
  * [MAC](#mac-media-access-control) destination d'un message

* **un câble RJ45**, c'est juste un câble qui est fait pour faire passer des trames Ethernet :)
  * d'ailleurs on les appelle parfois "câbles Ethernet" !
  * **donc rien besoin de plus que d'un câble pour créer un réseau**

---

#### *LACP* : Link Aggregation Control Protocol
* protocole ouvert permettant l'agrégat de plusieurs liens réseau en un unique lien virtuel
* cela permet d'accéder à des fonctionnalités de répartition de charges et de tolérance de pannes sur de lien, mais aussi d'en augmenter la bande passante
* [cours dédié](/cours/1.md#lacp)

---

#### *STP* : Spanning-Tree Protocol
* protocole ouvert permettant d'éviter les boucles réseau
* les switches de l'infrastructure qui utilise STP crée un consensus
* les switches se mettent d'accord pour désactiver certains ports de certains switches et ainsi éviter complètement les boucles
* [cours dédié](/cours/1.md#stp)

---

#### *VLAN* : Virtual Local Area Network
* permet d'isoler au niveau 2 des hôtes branchés sur le même réseau
* cela est possible en utilisant de nombreux switches (au moins un/LAN) mais c'est très coûteux ; au niveau de la charge de maintien, aussi bien que financièrement
* [cours dédié](/cours/1.md#vlan)

---

### Couche 2/3 du modèle OSI

#### *ARP* : Address Resolution Protocol

#### A. Concept

**Le protocole ARP permet de faire la liaison entre [adresses IP](/memo.lexique.md#ip-internet-protocol-v4) et [adresses MAC](/memo/lexique.md#mac-media-access-control).**
* pour joindre quelqu'un sur le même réseau que vous : **il vous faut connaître la MAC de ce "quelqu'un"**
  * ARP va permettre de demander la MAC de quelqu'un si l'on connaît son IP
  * un hôte peut aussi utiliser ARP afin d'informer "gratuitement" les autres hôtes qu'il possède telle IP, associée à telle MAC
* tous les équipements qui utilisent le [protocole IP](#ip-internet-protocol-v4) (équipement de niveau 3 au moins) les PCs/serveurs/routeurs/etc. possèdent une **table ARP**
  * elle contient les machine connues
  * une "machine connue" = on connaît son IP, et on connaît la MAC associée à cette IP
    * c'est à dire des infos comme "à l'IP `10.1.1.1` se trouve la MAC `10:10:10:10:10:10`"

#### B. Quand est-ce que ARP est utilisé ?

* si :
  * vous avez besoin de joindre quelqu'un sur le même réseau que vous
  * vous n'avez jamais joint ce quelqu'un auparavant
  * vous connaissez son IP
* alors :
  * il va vous falloir la MAC de ce quelqu'un
  * pour connaître cette MAC, vous allez effectuer une requête **ARP Request**
  * la machine qui porte l'IP en question, vous répondra avec un **ARP Reply**
  * et vous allez écrire la réponse dans votre **Table ARP**

> **La table ARP, c'est un tableau où vous enregistrer l'adresse de vos voisins. Easy m'kay ?**

#### C. ARP `request` et ARP `reply`

* **"machine1" veut communiquer avec "machine2"**
* **ARP request** (envoyé par "machine1") :
  * message très simple qui permet de demander la MAC quand on connaît l'IP
    * le message ressemble par exemple à : 
      * "SVP tout le monde,
      * je suis l'IP 192.168.10.10,
      * qui porte l'IP 192.168.10.1 ?"
* **ARP reply** (envoyé par "machine2") :
  * message très simple qui permet d'informer un hôte d'une association MAC <> IP
    * le message ressemble par exemple à : 
      * "Salut 192.168.10.10,
      * c'est moi 192.168.10.1,
      * ma MAC c'est ca:fe:ca:fe:ca:fe"
* après cet échange
  * "machine1" connaît la MAC de "machine2"
  * "machine1" écrit dans sa **table ARP** la ligne suivante 
    * `192.168.10.1 : ca:fe:ca:fe:ca:fe`

> **Le protocole ARP permet de connaître les adresses MAC des machines sur un réseau donné**

#### D. Adresse MAC de broadcast

* il existe une adresse  MAC "spéciale" : `ff:ff:ff:ff:ff:ff`
  * c'est l'adresse **MAC de broadcast**
* si vous envoyez un message à cette adresse MAC, **toutes les machines du même segment réseau recevront le message aussi**
* **quand vous envoyez un ARP Request, vous l'envoyez à l'adresse MAC de broadcast**
  * comme ça, tout le monde reçoit votre message
  * et parmi "tout le monde", y'a la personne qui vous intéresse
  * cette personne vous répondra

#### E. Exemple concret

* deux machines sont connectées avec un câble (et c'est tout) :
  * les machines n'ont jamais communiqué

Machines | IP | MAC
--- | --- | ---
`host1` | `192.168.56.101/24` | `11:11:11:11:11`
`host2` | `192.168.56.102/24` | `22:22:22:22:22`

1. `host1` veut envoyer un `ping` à `host2`
2. `host1` tape la commande suivante :
    * `ping 192.168.56.102`
3. `host1` regarde sa table ARP
    * elle est vide
    * il ne connaît pas la MAC associée à `192.168.56.102`
4. `host1` effectue une requête ARP (en broadcast)
    * `host1` envoie "Qui a l'IP `192.168.56.102` à `ff:ff:ff:ff:ff:ff`
    * `host2` reçoit le message et reconnaît que quelqu'un lui demande sa MAC
5. `host2` répond à la requête
    * `host2` envoie "C'est moi, `22:22:22:22:22:22` qui possède l'IP `192.168.56.102`
    * `host1` reçoit la réponse
6. `host1` ajoute une ligne dans sa table ARP
    * `host2 : 22:22:22:22:22:22`
7. `host1` fait partir son `ping` vers `22:22:22:22:22:22`

---

### Couche 3 du modèle OSI

#### *IP* : Internet Protocol v4

> On parle ici d'IPv4 (on verra peut-être un peu d'IPv6 ensemble)

* un *message IP* est un **paquet IP**
* protocole utilisé pour discuter à travers des réseaux
* une *adresse IP* peut être "portée" par une *carte réseau*
* une *adresse IP* est composée de 32 bits
  * par exemple : `192.168.1.1`
* pour comprendre l'IP on a besoin du [masque de sous-réseau](#masque-de-sous-r%C3%A9seau) qui lui est *obligatoirement* associé
* une adresse IP se trouve dans un réseau donné. Ce réseau est identifié par une [adresse de réseau](/memo/lexique.md#adresse-de-réseau)
* la décomposition d'une *adresse IP* est vue dans [la section subnetting](#subnetting)
* il existe des plages réservées d'adresses IP
  * entre autres, [les adresses privées et publiques](#ip-privéespubliques)

---

### Couche 4 du modèle OSI

#### *TCP* : Transmission Control Protocol

* un *message TCP* est un **datagramme TCP**
* permet d'établir un tunnel entre deux personnes, généralement un [client](/memo/lexique.md#clientserveur) et un [serveur](/memo/lexique.md#clientserveur)
* une fois le tunnel établi, le client et le serveur peuvent échanger des données
* voyez TCP comme un échange de messages (comme des textos) **avec accusé de réception**
* **on utilise TCP lorsqu'on veut une connexion stable, même si elle est un peu plus lente**
* une connexion HTTP utilise un tunnel TCP par exemple
* un tunnel TCP se crée entre un [port TCP](/memo/lexique.md#ports) d'une machine et le [port TCP](/memo/lexique.md#ports) d'une autre machine

#### *UDP* : User Datagram Protocol

* un *message UDP* est un **datagramme UDP**
* permet d'échanger des données, générélament entre un client et un serveur
* aucun tunnel n'est établi, les données sont envoyées **sans accusé de réception**
* **on utilise UDP lorsqu'on s'en fiche de perdre certains messages sur la route, afin d'optimiser la vitesse de transport**
* UDP est par exemple très utilisé dans les jeux en ligne (typiquement pour des FPS en ligne)
* un tunnel UDP se crée entre un [port UDP](/memo/lexique.md#ports) d'une machine et le [port UDP](/memo/lexique.md#ports) d'une autre machine

> On peut lister les ports TCP et UDP utilisés par une machine avec la commande [`ss` (ou `netstat`)](/memo/linux_network_memo.md#netstat-ou-ss).

---

### Au dessus de 4 (couches "applicatives" qui transportent de la donnée)

#### *DHCP* : Dynamic Host Configuration Protocol

DHCP est un protocole réseau permettant d'attribuer automatiquement (de façon "dynamique") des adresses IPs aux machines d'un réseau.  

Le protocole DHCP est mis en place par une relation entre un serveur DHCP et des clients qui n'ont pas d'adresses IP, le tout sur un réseau donné.  

Lorsqu'un client arrive sur un réseau, il ne connaît ni la MAC ni l'IP du serveur DHCP. L'échange afin qu'il obtienne son IP se passe comme suit (c'est l'échange **DORA**) :  

**1. Discover**  
  * un client rejoint un réseau (connexion à un réseau WiFi, ou par câble)
  * le client envoie [un message en broadcast](./lexique.md#adresse-de-diffusion-ou-broadcast-address) (à l'adresse MAC `ff:ff:ff:ff:ff:ff`)
  * ce message a pour but de découvrir le réseau à la recherche d'un serveur DHCP  

**2. Offer**  
  * un serveur DHCP reçoit le **Discover**
  * il répond un message **Offer**
    * il contient l'IP du serveur DHCP
    * et une IP que le serveur propose au client  

**3. Request**  

  * le client reçoit le message **Offer**
  * il répond au serveur un message **Request**
    * ceci a pour but de demander au serveur si, en effet, il peut prendre l'IP proposée
    * la présence de ce message est surtout utilisée à des fins de sécu, et afin de traiter le cas où plusieurs serveurs DHCP sont présents  

**4. Acknowledge**  
  * le serveur reçoit le message **Request**
  * il répond un **Acknowledge** 
    * il valide l'attribution de l'IP au client
    * et il crée un **bail DHCP**  

**5. Le client a une IP dans le bon réseau**
  * le client stocke un fichier appelé "bail DHCP" en local
    * il contient des infos relatives à l'adresse récupérée/le réseau (serveur DNS du réseau, adresse du DHCP, durée de vie, etc.)
  * le serveur stocke un fichier appelé "bail DHCP" en local
    * il contient l'association IP <> MAC pour chaque client

Un bail DHCP contient plusieurs informations :
* tel client possède actuellement telle adresse IP
  * les clients sont identifiés par leurs MAC
* date d'expiration

Enfin, un serveur DHCP permet aussi de distribuer d'autres informations, par exemple : 
* adresse de [la passerelle](./lexique.md#passerelle-ou-gateway) du réseau
* adresse du serveur [DNS](./lexique.md#dns-domain-name-system) du réseau

---

#### *DNS* : Domain Name System
* protocole utilisé pour associer *un nom d'hôte et un nom de domaine* à une adresse IP
* les *serveurs DNS* sont des serveurs à qui on peut poser des questions
  * "donne moi le nom de domaine associé à telle IP"
  * "donne moi l'IP associée à tel nom de domaine"
* des outils comme [`nslookup` ou `dig`](/memo/linux_network_memo.md#nslookup-ou-dig) peuvent être utilisés pour interroger des serveurs DNS à la main

#### *HTTP* : HyperText Transfer Protocol
* protocole utilisé pour discuter avec des serveurs web
* comme beaucoup de protocoles de communication, HTTP fonctionne avec un principe de question/réponse
  * le client effectue une requête HTTP à un serveur web
    * = il demande une page spécifique
  * le serveur répond à la requête du client dans une réponse HTTP
    * = il donne une page HTML + du CSS et du JS au client

#### *SSH* : Secure SHell
* protocole/outil utilisés pour se connecter à distance de **façon sécurisée** sur un équipement
* on peut alors contrôler l'équipement en passant par le réseau
  * l'équipement distant doit faire tourner une application : un **serveur SSH** 
    * souvent le serveur SSH écoute sur le port [TCP](/memo/lexique.md#tcp-transmission-control-protocol) numéro 22
  * votre PC doit posséder un **client SSH** :
    * la commande `ssh` (simple, puissant, léger)
    * ou [Putty](https://www.putty.org/) (sur Windows, quand la commande `ssh` n'est pas dispo)

---

## Sigles/Acronymes

---

### *CIDR* : Classless Inter-Domain Routing
* de façon simple, c'est le `/24` dans `192.168.1.1/24` par exemple
* un `/24` veut dire "24 bits à 1" et correspond donc au masque `11111111.11111111.11111111.00000000` soit `255.255.255.0`

---

### *FQDN* : Fully Qualified Domain Name
* c'est le nom complet d'un hôte (= d'une machine) sur le réseau
* il est la concaténation du nom d'hôte et du domaine
* [cf. la section dédiée aux noms de domaine](#noms-de-domaine)

---

### *LAN* : Local Area Network
* réseau local
* les équipements qui s'y trouvent portent des [adresses privées](#ip-privéespubliques)

---

### *MAC* : Media Access Control
* on parle l'ici de *l'adresse MAC* ou *adresse physique*
* elle est *obligatoirement* portée par une *carte réseau*
* sur une carte physique, elle est gravée sur la carte (on ne peut pas la changer)
* l'*adresse MAC* est composée de 12 caractères hexadécimaux (par exemple `D4-6D-7D-00-15-3F`)
* les adresses MAC servent à envoyer des messages directs aux machines sur le même réseau que nous, elles sont utilisées par [le protocole Ethernet](#ethernet)
  
> Pas besoin d'avoir des adresses IP pour communiquer si on est sur le même réseau ! 

---

### *RFC* : Request For Comments
* document texte qui définit une notion, un concept, un protocole (principalement utilisé en informatique)
* les RFCs peuvent écrit par n'importe qui et sont des documents publics
* comme son nom l'indique, une RFC a pour but d'être lue et commentée
* si les gens la lisent, la commentent, la complètent, une RFC finit par contenir des choses intéressantes
* **voyez-les comme des manuels pour construire des choses, ce sont juste des plans**
  * un plan pour construire une messagerie sécurisée, par exemple !
* les protocoles que vous connaissez ont tous été définis dans des RFCs, entre autres :
  * [IP](#ip-internet-protocol) : [RFC 791](https://tools.ietf.org/html/rfc791)
  * Allocation d'adresses privées : [RFC 1918](https://tools.ietf.org/html/rfc1918)
  * [HTTP](#http-hypertext-transfer-protocol) : [RFC 2616](https://tools.ietf.org/html/rfc2616)
  * [TCP](#tcp-transmission-control-protocol) : [RFC 793](https://tools.ietf.org/html/rfc793)
  * [Ethernet](#ethernet) : [RFC 826](https://tools.ietf.org/html/rfc826), [RFC 894](https://tools.ietf.org/html/rfc894)
    * un extrait de l'intro, c'est cadeau : 
> "The world is a jungle in general, and the networking game contributes many animals."

---

### *WAN* : Wide Area Network
* réseau étendu
* celui que vous utilisez le plus est Internet
  * les équipements qui s'y trouvent portent des [adresses publiques](#ip-privéespubliques)

---

## Notions

### Adresse de réseau
* adresse qui définit un réseau
* elle ne peut pas être utilisée comme adresse d'un hôte
* elle correspond à la première adresse disponible d'un réseau
* exemple :
  * pour l'adresse IP `192.168.1.37/24`, l'adresse de réseau est `192.168.1.0/24`

---

### Adresse de diffusion (ou *broadcast address*)

#### Pour le [protocole Ethernet](#ethernet)

* dernière MAC "possible" : `ff:ff:ff:ff:ff:ff`
* elle n'est portée par aucun équipement
* envoyer une trame Ethernet à cette [adresse MAC](#mac-media-access-control) permet d'envoyer un message à tous les autres équipements sur le réseau où l'on se trouve
* si un switch reçoit une trame avec pour destination `ff:ff:ff:ff:ff:ff`, il renverra ce message sur tous ses ports

#### Pour le [protocole IP](#ip-internet-protocol-v4)

* dernière adresse d'un réseau
* est utilisée pour envoyer un message à tous les hôtes d'un réseau
* elle ne peut pas être utilisée comme adresse d'un hôte
* **l'adresse de broadcast n'est portée par aucune machine**, c'est une adresse réservée dans tous les LANs du monde :)
* exemple :
  * pour l'adresse IP `192.168.1.37/24`, l'adresse de broadcast est `192.168.1.255/24`

---

### Binaire
* c'est la base 2 des mathématiques
* nous sommes habitués à compter en base 10
* n'importe quelle base est possible, certaines sont beaucoup utilisées en informatique
  * binaire (base 2)
  * décimal (base 10)
  * octal (base 8)
  * héxadécimal (base 16)
  * base 64 (oui oui, base 64)
  * pour les curieux : en fait l'ASCII normal (pas la table étendue), c'est juste une base 128 hein :)
* il faut le maîtriser pour pouvoir faire du [subnetting](#subnetting)

---

### Carte réseau (ou interface réseau)
* aussi appelée *NIC* pour *Network Interface Card*
* carte physique dans une machine (ou virtuelle)
* porte forcément une [adresse MAC](#mac-media-access-control)
  * pour une interface physique, la MAC est gravée physiquement sur le périphérique : on ne peut pas la changer
* peut porter une [adresse IP](#ip-internet-protocol) et ainsi "être dans un réseau"
* dans nos PCs du quotidien, on en a au moins une : la carte WiFi

---

### Client/Serveur

* la plupart des connexions que nous effectuons tous les jours sont des connexions de types client/serveur
* l'idée est la suivante
  * un serveur propose un service
  * un client veut accéder au service : il se connecte au serveur
* souvent, dans un réseau IP :
  * **le serveur** : la machine qui propose un service
    * possède une [carte réseau](./lexique.md#carte-réseau-ou-interface-réseau) et est branché à un réseau
    * possède une adresse IP sur cette [carte réseau](./lexique.md#carte-réseau-ou-interface-réseau)
    * a une application qui tourne : le service à fournir 
    * l'application écoute sur un [port](./lexique.md#ports) : [TCP](./lexique.md#tcp-transmission-control-protocol) ou [UDP](./lexique.md#udp-user-datagram-protocol) le plus souvent
  * **le client** : la machine qui veut accéder au service
    * possède une [carte réseau](./lexique.md#carte-réseau-ou-interface-réseau) et est branché à un réseau
    * possède une adresse IP sur cette [carte réseau](./lexique.md#carte-réseau-ou-interface-réseau)
    * connaît une route pour joindre le serveur
    * veut accéder à un service 
    * il connaît l'adresse du serveur et le port sur lequel il doit demander le service
    * il utilise un client qui parle la même langue que le serveur
    * il interroge le serveur avec son client, en passant par sa [carte réseau](./lexique.md#carte-réseau-ou-interface-réseau)
* par exemple, pour les serveurs web :
  * ils écoutent sur les ports  [TCP](./lexique.md#tcp-transmission-control-protocol) 80 (HTTP) et 443 (HTTPS) le plus souvent
  * quelques noms de serveurs Web connus : [Apache](https://httpd.apache.org/), [NGINX](https://www.nginx.com/)
  * quelques noms de clients Web connus : [Firefox](https://www.mozilla.org/fr/firefox/new/), [Chrome](https://www.google.com/chrome/), [curl](./lexique.md#curl-et-wget)
  
* **de façon simple**
  * pour accéder à n'importe quel serveur, il vous faut un client
  * pour accéder à un serveur Web, on a des clients Web : [Firefox](https://www.mozilla.org/fr/firefox/new/), [Chrome](https://www.google.com/chrome/), [curl](./lexique.md#curl-et-wget)
  * **le client doit connaître l'IP et le port du serveur qu'il veut joindre**
  * quand vous vous connectez à un port, vous aussi vous ouvrez un port sur votre machine
  * schéma moche :
```
 ___________________                       ___________________
|     SERVEUR       |                     |      CLIENT       |
|                   |                     |                   |
|   serveur_web<--IP:PORT<----réseau<---IP:PORT<---firefox    |
|                   |                     |                   |
|___________________|                     |___________________|
```

**On peut visualiser les connexions clientes ("established") ou les connexions serveur ("listening") d'une machine à l'aide de [la commande `ss` (ou `netstat`)](/memo/linux_network_memo.md#netstat-ou-ss).**

---

### Encapsulation

> Bon, ça, sans un tableau blanc et un schéma, c'est chaud de faire un cours utile et clair. I'll try.

#### Concept

Quand un message circule sur le réseau, celui-ci est "encapsulé" de la même façon qu'une lettre postale l'est (enveloppe + sac pour le centre de tri).  

Ce mécanisme d'encapsulation permet d'acheminer de façon simple des messages d'un point à un autre.  

On a déjà joué avec [TCP](#tcp-transmission-control-protocol) ([`netcat`](/memo/linux_network_memo.md#nc-ou-netcat), et même [`curl`](/memo/linux_network_memo.md#curl-et-wget) !), avec [IP](#ip-internet-protocol) (adresses IP, etc), et avec Ethernet (adresses MAC). Le principe est le suivant : 
* le message TCP contient la notion de ports
  * c'est le numéro de la porte sur la machine de destination.
* quelle machine ? 
  * C'est l'IP qui s'en charge. Le message IP permet d'acheminer le message jusqu'à la machine de destination. 
* comment atteindre cette machine ? 
  * En passant par des machines intermédiaires (ou pas)
  * c'est Ethernet qui s'en charge, avec l'utilisation des adresses MAC

Le but c'est donc : 
* créer un message, de la donnée, par exemple "toto"
* le mettre dans un message TCP, avec un port de destination
* mettre le message TCP dans un message IP, avec une IP de destination
* et enfin, mettre le message IP dans un message Ethernet, avec une MAC de destination

**Allez quoi c'est juste une boîte dans une boîte dans une boîte. Z'avez déjà vu des poupées russes non ? Bah pareil.**

#### Vocabulaire

* un message [TCP](#tcp-transmission-control-protocol), c'est un **datagramme TCP** (idem pour UDP).
* un message [IP](#ip-internet-protocol), c'est un **paquet IP**
* un message [Ethernet](#ethernet), c'est une **trame Ethernet**

**Le réseau fonctionne donc en mettant de la donnée dans des datagrammes dans des paquets IP dans des trames Ethernet. Ouais ouais.**

![Mindblown](/memo/pic/lexique/encapsulation-mindblown.gif "Mindblown.")

#### UN SCHEMA POUR DIGERER TOUT CA

Sur le schéma qui suit :
* deux réseaux, `net1` et `net2` ayant respectivement pour [adresses de réseau](#adresse-de-réseau) `10.1.0.0/24` et `10.2.0.0/24`
* un [*client*](#clientserveur) est dans `net1` : `10.1.0.10/24`
* un [*serveur*](#clientserveur) est dans `net2` : `10.2.0.10/24`
* un [routeur](#routeur)
  * est dans `net1` : `10.1.0.254`
  * est dans `net2` : `10.2.0.254`
  * sert de [passerelle](#passerelle-ou-gateway) pour le [*client*](#clientserveur) et le [*serveur*](#clientserveur)

1. On admet que *client* est déjà connecté à *serveur*
    * *serveur* a executé [la commande `nc -l 9999`](/memo/linux_network_memo.md#nc-ou-netcat)
      * pour écouteur sur un port et attendre
    * *client* a executé [la commande `nc 10.2.0.10 9999`](/memo/linux_network_memo.md#nc-ou-netcat)
      * pour se connecter au port ouvert sur *serveur*
2. *client* envoie "toto" à serveur dans le chat [`netcat`](/memo/linux_network_memo.md#nc-ou-netcat) :

![Circulation d'une trame sur le réseau.](/memo/pic/lexique/encapsulation-circulation-de-trame.png "Circulation d'une trame sur le réseau.")

---

### IP Privées/Publiques
* les **IP privées** sont utilisées pour attribuer des adresses dans des LAN
  * les IP privées sont définies à l'intérieur des réseaux suivants : 
    * `10.0.0.0/8`
    * `172.16.0.0/12`
    * `192.168.0.0/16`
* les **IP publiques** sont utilisées pour attribuer des adresses sur Internet
  * ce sont toutes celles qui ne sont pas contenues dans les plages privées
  * on ne peut pas avoir n'importe quelle IP publique, ce sont des sociétés appelées [RIR](https://en.wikipedia.org/wiki/Regional_Internet_registry) qui s'occupent de gérer les IP publiques et de les distribuer (entre autres)
  
* **en réalité** il existe d'autres **plages d'adresses IP réservées**. [Wikipédia est votre ami](https://en.wikipedia.org/wiki/Reserved_IP_addresses).

---

### Loopback
* quand on parle de Loopback on parle d'une interface réseau
* il existe au moins une Loopback sur tous les équipements
  * sur vos PCs elle porte l'IP `127.0.0.1`
  * parfois elle est gérée différemment par rapport aux autres interfaces (vous ne la voyez pas sur Windows avec un `ipconfig`)
* **une interface de loopback permet uniquement de se joindre soi-même**
  * en fait c'est juste qu'elle porte une IP dans un `/32` le plus souvent, un masque un peu particulier, notamment utilisé à cet effet :)

> Go test un `ping 127.0.0.1` vous aurez jamais vu un `ping` si rapide. Normal : vous pingez votre propre machine, sans passer par le réseau

---

### Masque de sous-réseau
* permet d'extraire l'[adresse de réseau](#adresse-de-réseau) d'une adresse IP
* se présente sous sa forme classique `255.255.255.0` ou en notation [CIDR](#cidr-classless-inter-domain-routing) `/24`
* **les trois affirmations suivantes sont parfaitement équivalentes :**
  * "j'ai un réseau en `255.255.255.0`"
  * "j'ai un `/24`"
  * "j'ai un réseau avec 256 adresses possibles"
  * "j'ai un réseau avec 254 adresses dispobibles"
* la décomposition d'une IP est vue dans [la section subnetting](#subnetting)

---

### Noms de domaine
**Le *nom de domaine* d'une machine est le nom d'une machine sur le réseau**. Attention, le *nom de domaine* est un terme souvent mal utilisé. En effet, en termes techniques, on distingue :
* le *nom d'hôte* ou *hostname* qui est le nom de la machine elle-même
  * exemple : `www` pour un serveur web
* le *(nom de) domaine* ou *domain (name)* est plus ou moins le nom du réseau
  * exemple : `google.com`
* le [*FQDN*](./lexique.md#fqdn--fully-qualified-domain-name) est la concaténation des deux (*hostname* + *domain*)
  * exemple : `www.google.com`
* en résumé :
  * **hostname + domain = FQDN**


**La configuration du FQDN d'une machine se fait en deux étapes** : 

**1. [Donner un nom à la machine](./procedures.md#changer-son-nom-de-domaine)**
* ceci permet à la machine elle-même de connaître un nom
* c'est effectué **sur la machine elle-même**
  
**2. Configurer un outil pour que les autres machines connaissent son nom**
* c'est effectué **à l'extérieur de la machine**
* on a besoin d'un annuaire, auquel tout le monde a accès :
  * [le fichier `hosts`](./procedures.md#editer-le-fichier-hosts)
    * il est présent sur toutes les machines, tous les OS
    * **c'est un annuaire local**
    * il n'est valable que pour la machine qui le possède
  * [*DNS*](./lexique.md#dns--domain-name-system)
    * on en reparlera plus en détails
    * par *DNS*, on entend :
      * le protocole DNS
      * des serveurs DNS
      * une architecture de serveur DNS
    * **il peut servir d'annuaire commun**

**NOTE** : *une machine peut être jointe par son nom, même si elle ne le connaît pas elle-même. Autrement dit, dans les deux étapes citées ci-dessus, seule la deuxième est strictement obligatoire*  
***Ne me faites pas dire ce que je n'ai pas dit**, le 1 reste important dans beaucoup de cas.*

---

### Pare-feu ou *firewall*
* présent sur la plupart des équipements (PCs, serveurs, etc)
* peut exister sous la forme d'un équipement physique
  * c'est alors un routeur, qui a la possibilité de filtrer les trames qui le traversent
  * oui un firewall physique **est** un routeur
* c'est une application qui permet de filtrer le trafic réseau
  * filtrage du trafic qui entre sur la machine
    * par exemple le firewall Windows bloque le `ping` entrant par défaut
  * filtrage du trafic qui sort de la machine
    * surtout utilisé sur des PCs d'entreprise ou sur des serveurs
* un firewall filtre principalement (mais pas que) en fonction des [adresses IP](/memo/lexique.md#ip-internet-protocol-v4) et des ports [TCP](/memo/lexique.md#tcp-transmission-control-protocol)/[UDP](/memo/lexique.md#udp-user-datagram-protocol)

---

### Passerelle ou *Gateway*
* la *passerelle* est un noeud agissant comme pivot, et elle permet de sortir d'un réseau (de chez vous vers Internet par exemple)
* il existe des réseaux sans passerelle
* la passerelle possède souvent l'IP juste avant [l'adresse IP de *broadcast*](/memo/lexique.md#adresse-de-diffusion-ou-broadcast-address), mais pas toujours (ce n'est pas le cas à Ingésup par exemple)
* **la passerelle est une machine. L'adresse IP de gateway est donc l'adresse IP d'une machine présente sur le même réseau que vous.** C'est une adresse IP réelle, contrairement à l'adresse de [*broadcast*](/memo/lexique.md#adresse-de-diffusion-ou-broadcast-address)
* **un réseau qui ne souhaite pas être connecté à d'autres réseaux n'a pas besoin de passerelle**

---

### Ports

* Un port est un point d'entrée unique dans une machine.   
* Il en existe un nombre fixe (65536) **pour chacune des interfaces réseau d'une machine**.  
* Les ports sont utilisés pour permettre à deux machines d'entretenir toutes discussions un peu complexes, longues, ou nécessitant un peu de sécurité (nous reviendrons plus tard sur ce point).  
* Si une IP est l'adresse de quelqu'un dans une rue, alors les ports sont des portes dans sa maison. Ui ça fait beaucoup 65536 portes. Grosse soirée quoi.  
* **De façon simple, un "serveur" c'est une machine sur laquelle tourne une application qui "écoute" sur un port donné, sur une IP donnée (noté `IP:PORT`).**
  * quand on veut aller sur "le port 443 du serveur `192.168.1.13`" on le note `192.168.1.13:443`
  * Par exemple, le serveur web de www.google.com écoute sur son IP publique et sur le port 443 de cette IP (443 c'est HTTPS, c'est juste une convention).
  * allez sur `https://www.google.com` c'est pareil qu'aller sur `https://www.google.com:443`. Notre navigateur cache le 443 parce que c'est pas beau. Par convention on a dit, "si c'est de l'`https` alors on cache le 443". 
* **Tous les serveurs écoutent sur au moins une IP et un Port**. C'est ça qui fait qu'ils peuvent accepter des connexions pour des clients. "Ecouter sur un port" = "ouvrir une porte", m'okay ? :)  
* Derrière un port, on met une application. Un serveur Web par exemple. Un schéma moche :
```
      Serveur 
---------------------
|                    |
|                    |
|  site web <------IP:443<-------  Client
|                    |
|____________________|
```

* il existe deux types de ports : [**TCP**](#tcp-transmission-control-protocol) et [**UDP**](#udp-user-datagram-protocol)
* **cas typique**
  * un serveur Web utilise le protocole HTTPS (port `443` en [TCP](#tcp-transmission-control-protocol)) pour donner des pages web a ses clients
  * afin d'être disponible, on demande au serveur web d'écouter sur une IP et un Port
  * un client veut visiter le site, il utilise alors un client Web, comme Chrome, Firefox, ou encore `curl` ou `wget`
  * ce client utilise la chaîne de connexion suivante pour s'y connecter : `https://IP_SERVEUR:443`
* **avec netcat** 
  * `netcat` est une application très simpliste qui permet d'écouter sur un port (= être le serveur), ou de se connecter à un port (= être le client), de façon arbitraire

---

### Port-channel

On parle d'agréger des interfaces physiques (d'un switch par exemple, avec LACP par exemple) en une unique interface virtuelle.

Alors là c'est le dawa, y'a quinze termes pour désigner (presque) la même chose :
* *etherchannel* : **technologie utilisée pour agréger des interfaces**
* *port-channel* ou *bond* ou *teaming* ou *LAG* (*Link aggregration group*) : **nom de l'interface virtuelle**
* *link agregation* ou *port bundling* ou *NIC teaming* ou *NIC/network bonding* : **l'action d'agréger plusieurs interfaces** (l'action de créer un *port-channel* donc)
* [*LACP*](#lacp-link-aggregation-control-protocol), *PAgP* : **protocoles permettant de créer des *port-channels***

> Souvent, *etherchannel* et *port-channel* sont utilisés (à tort) de façon interchangeable

> Sur des équipements Cisco on parle de *port-channel* et d'*etherchannel* principalement, avec les protocoles *LACP* (ouvert) ou *PAgP* (propriétaire)

---

### Routage ou *routing*
* c'est le fait de mettre en place et de configurer un [routeur](#routeur)

---

### Routeur
* **Très important**
* un routeur est un équipement réseau 
  * c'est un PC quoi, mais optimisé :)
* il est au milieu de plusieurs réseaux, au moins deux (sinon c'est pas un routeur !)
  * **pour rappel** : "être dans un réseau" = "être branché (câble ou wifi) **+** posséder une carte réseau **+** avoir une IP dans le réseau donné"
  * donc il a au moins deux interfaces réseaux
  * et donc au moins deux adresses IPs ! :)
  
```
 ____________                      ____________
|  Réseau 1  |                    |  Réseau 2  |
|            |------ Routeur -----|            |
|____________|                    |____________|
```

* **il permet aux gens du réseau 1 d'aller vers le réseau 2, et vice-versa**

* la [passerelle](#passerelle-ou-gateway) d'un réseau, c'est souvent un routeur !

* son rôle est de connaître des "routes" et d'en faire profiter les réseaux auxquels il est connecté
  * une "route" est un chemin pour aller vers un réseau
  * par exemple, une route c'est : 
    * "pour aller dans `192.168.1.0/24`,
    * tu passes par l'[interface réseau](#carte-réseau-ou-interface-réseau) numéro 4,
    * en passant par la [passerelle](#passerelle-ou-gateway) `192.168.0.254/24`"

* il existe une route spéciale : la **route par défaut** 
  * c'est la route à prendre quand on connaît pas de routes spécifiques pour une adresse donnée
  * c'est le panneau "Toutes directions" quoi !
  
* **le routeur est un mec sympa : il connaît les routes, mais surtout, il vous permet d'y accéder**
  * chez vous, le routeur c'est votre Box
  * elle connaît une route pour aller dans votre [LAN](#lan--local-area-network) et une route pour aller sur Internet (le [WAN](#wan-wide-area-network))
  * et votre box est sympa : elle vous a dit que si vous aviez besoin d'aller sur internet, vous pouviez passer par elle

* **EDIT** : un routeur est un équipement qui fait de l'*IP Forwarding* :
  * ça veut dire qu'il peut traiter des paquets IP qui ne lui sont pas destinés
  * afin de les faire transiter de réseau en réseau
  * par exemple, vos paquets, quand **vous** parlez à google, ils sont pas destinés au routeur, mais à **vous**. Pourtant c'est bien le routeur qui les traite avant de vous les donner. M'kay ?
  * on reviendra plus tard sur le *NAT*
  * **il suffit d'activer ça sur votre PC pour qu'il devienne un routeur** 

---

### *Stack réseau* ou *stack TCP/IP* ou Pile réseau
* désigne toutes les applications d'une machine qui s'occupent du réseau
* si quand vous tapez `ipconfig` il se passe quelque chose, bah y'a bien une application qui s'en occupe
* désigne des choses bien différentes suivant les OS
* on utilise le terme *stack réseau* pour désigner tout ce qui touche de près ou de loin au réseau sur une machine
  * gestion d'IP
  * gestion d'interfaces
  * firewall
  * et plein plein d'autres choses

---

### Subnetting
* c'est le fait de découper un réseau en plusieurs sous-réseaux, et d'attribuer des IPs
* par exemple, un `/24` contient deux `/25`
  * `192.168.1.0/24` est la réunion de `192.168.1.0/25` et `192.168.1.128/25`
* il existe des tonnes d'outils permettant d'assister le subnetting en évitant de devenir fou avec le binaire, comme [celui-ci](http://www.davidc.net/sites/default/subnets/subnets.html)
* le [binaire](#binaire) est nécessaire pour faire du subnetting


* décomposition d'une adresse IP `192.168.3.17/22` :

**1.** `/22` est la *notation [CIDR](./lexique.md#cidr-classless-inter-domain-routing)* d'une IP et de son masque. Une autre façon de le dire est :
  * adresse IP : `192.168.3.17` ou en binaire `11000000.10101000.00000011.00010001`
  * masque de sous-réseau : `255.255.252.0` ou en binaire `11111111.11111111.11111100.00000000`  
  
**2.** Notons l'un sous l'autre afin de correctement visualiser l'utilisation du masque :
```
A. 11000000.10101000.00000011.00010001  (adresse IP)
B. 11111111.11111111.11111100.00000000  (masque de sous-réseau)
```  
  
**3.** Pour **calculer l'adresse de réseau**, il faut regarder les deux lignes en même temps (comme si on appliquait un "masque" à l'adresse IP, vraiment) : 
  * si la ligne B (le masque) contient un 1, alors on "garde" le chiffre correspondant de la ligne A
  * si la ligne B (le masque) contient un 0, alors on "jette" le chiffre correspondant de la ligne A, et on note 0
  * ici, on obtient : 
```
                             adresse
        adresse réseau         hôte
   .----------------------..---------.
A. 11000000.10101000.00000011.00010001  (adresse IP)
B. 11111111.11111111.11111100.00000000  (masque de sous-réseau)
C. 11000000.10101000.00000000.00000000  (adresse de réseau)
```  
**L'adresse de réseau de `192.168.1.37/22` est `11000000.10101000.00000000.00000000/22` soit `192.168.0.0/22`.**
Elle correspond dans notre exemple aux 22 premiers bits de l'adresse IP de départ, suivie de 0. Car c'est un `/22`.  

**4.** Pour **calculer l'adresse de broadcast**, on garde aussi les 22 premiers bits de l'adresse IP, mais cette fois, on comble avec des 1 :
```
                             adresse
        adresse réseau         hôte
   .----------------------..---------.
A. 11000000.10101000.00000011.00010001  (adresse IP)
B. 11111111.11111111.11111100.00000000  (masque de sous-réseau)
C. 11000000.10101000.00000000.00000000  (adresse de réseau)
D. 11000000.10101000.00000011.11111111  (adresse de brodcast)
```  
**L'adresse de broadcast du réseau `192.168.0.0/22` est `11000000.10101000.00000011.11111111/22` soit `192.168.3.255/22`.
Elle correspond dans notre exemple aux 22 premiers bits de l'adresse IP de départ, suivie de 1. Car c'est un `/22`.  

**5.** Souvent (mais pas tout le temps), la *gateway* ou *passerelle* porte l'adresse IP juste avant l'adresse de broadcast :
```
                             adresse
        adresse réseau         hôte
   .----------------------..---------.
A. 11000000.10101000.00000011.00010001  (adresse IP)
B. 11111111.11111111.11111100.00000000  (masque de sous-réseau)
C. 11000000.10101000.00000000.00000000  (adresse de réseau)
D. 11000000.10101000.00000011.11111111  (adresse de broadcast)
E. 11000000.10101000.00000011.11111110  (adresse de gateway)
```

On obtient alors : 
* Informations de base
  * A : adresse IP : `192.168.3.17/22`
  * B : masque de sous-réseau : `255.255.252.0`
* Informations déduites
  * C : adresse de réseau : `192.168.0.0/22`
  * D : adresse de broadcast : `192.168.3.255/22`
  * E : adresse de passerelle : `192.168.3.254/22` (probable)

**6.** Une fois ces notions en tête, on peut **calculer le nombre d'hôtes possibles** : 
  * notre réseau est un `/22` cela fait donc 10 bits disponibles pour nos adresses d'hôtes (l'IP est constituée de 32 bits, et 22 sont réservés à l'adresse réseau)
  * 10 bits disponibles, cela fait 2^10 possibilités, soit 1024 possibilités, **1024 "adresses possibles MAIS"** :
    * l'adresse de réseau n'est pas utilisable : 1023 possibilités
    * l'adresse de brodcast n'est pas utilisable : 1022 possibilités
    * si le réseau possède une gateway, son IP n'est pas libre : 1021 possibilités
  * en comptant l'adresse de gateway, on a **1021 adresses disponibles** ("possible" != "disponible")
  
* **NB**
  * la partie `adresse de réseau` ne change **jamais** pour toutes les adresse d'un réseau donné (on le voit bien dans le `5.`)
  * toutes les machines d'un même réseau peuvent communiquer
  * grâce à nos interfaces réseau, on peut porter des IPs, et ainsi être dans un réseau
  * **c'est bien le fait d'avoir une adresse qui nous rend "membre d'un réseau"**  

---

### Table de routage

Il existe une table de routage sur toutes les machines qui peuvent porter des IPs (PCs, routeurs, etc).

La table de routage indique au système par quelle interface et par quelle intermédiaire il doit passer pour atteindre un réseau donné. C'est le chemin à prendre pour atteindre une destination.

Chaque ligne dans une table de routage est appelée une **route**. Une **route** contient les informations suivantes : 
* réseau à joindre
* quelle [interface réseau](#carte-réseau-ou-interface-réseau) utilisée pour le joindre
* une machine intermédiaire pour joindre le réseau (si on est pas directement connecté au réseau en question) : **la [passerelle](#passerelle-ou-gateway) pour ce réseau**

Exemple : 
```
                      +-------+
                      |Serveur|
                  WAN |Reddit |
                      +---+---+
                          |
                          |
                          |
                    254+--+-----+              +--------+
          LAN1 +-------+Routeur1+--------------+Routeur2|  LAN2
192.168.1.0/24 |       +--------+              +--------+  192.168.2.0/24
               |            |254                    |254
               |1          2|                      1|
             +---+        +---+                   +---+
             |PC1|        |PC2|                   |PC3|
             +---+        +---+                   +---+
```

#### Table de routage du PC1 :

Route | Interface | Machine intermédiaire
--- | --- | ---
192.168.1.0/24 | 192.168.1.1/24 | Aucune (directement connecté)
192.168.2.0/24 | 192.168.1.1/24 | 192.168.1.254/24
Default | 192.168.1.1/24 | 192.168.1.254

On peut dire que, pour PC1, la passerelle par défaut est le routeur1, plus spécifiquement, le routeur1 à l'adresse `192.168.1.254/24`.  
Routeur1 est aussi sa passerelle pour le réseau `192.168.2.0/24`.

#### Table de routage du PC3 :

Route | Interface | Machine intermédiaire
--- | --- | ---
192.168.1.0/24 | 192.168.2.1/24 | 192.168.2.254/24
192.168.2.0/24 | 192.168.2.1/24 | Aucune (directement connecté)
Default | 192.168.2.1/24 | 192.168.2.254
